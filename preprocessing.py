from typing import List, Callable, Dict

import numpy as np
import pandas as pd
from scipy import signal
from toolz import curry

import util


def split_by_time(skip_distance: float, frame: pd.DataFrame) -> List[pd.DataFrame]:
    time_skips = [i for i in range(0, len(frame) - 1)
                  if (frame["time"].iloc[i + 1] - frame["time"].iloc[i]) > skip_distance]
    cuts = [0, *time_skips, len(frame)]
    return [frame.iloc[cuts[i]:cuts[i + 1]] for i in range(0, len(cuts) - 1)]




def apply_window(values: pd.Series) -> pd.Series:
    return values * signal.windows.tukey(len(values), alpha=0.2)


def get_points_of_interest(samples: pd.DataFrame):
    if "marked" in samples:
        return [i for i, is_of_interest
                in enumerate(samples["marked"])
                if is_of_interest]
    else:
        return None


@curry
def generate_windows(window_size,
                     window_spacing,
                     samples: pd.DataFrame) -> List[pd.DataFrame]:
    points_of_interest = get_points_of_interest(samples)

    return [samples.iloc[(i - window_size):i]
            for i in range(0, len(samples), window_spacing)
            if (i - window_size) > 0
            if (points_of_interest is None
                or util.contains(lambda point: (i - window_size) < point < i, points_of_interest))]
