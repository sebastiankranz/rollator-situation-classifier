# %%


from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.utils.multiclass import unique_labels

from classification import stratified_split_by_ratio
from feature_extraction import extract_features_dwt, extract_features_stats
from plotting import make_figure, normalized_confusion_matrix, \
    ConfusionMatrix
from read import SampleSource, load_windows
from save_session import open_shelve
from standardization import standardize_per_column
from util import (
    log_time,
)

# "linAccX", "linAccY", "linAccZ",
# "angVelZ"]  (Corrupted)


sources: List[SampleSource] = [
    SampleSource("Sebastian/step_up.csv", "step_up"),
    SampleSource("Sebastian/step_down.csv", "step_down"),
    SampleSource("Sebastian/idle.csv", "idle"),
    SampleSource("Sebastian/sitting.csv", "sitting"),
    SampleSource("Sebastian/sit_down.csv", "sit_down"),
    SampleSource("Other/2019_4_11_14_55_sitzen_nr1_runter.csv", "sit_down"),
    SampleSource("Other/2019_4_11_15_0_sitzen_nr2_runter.csv", "sit_down"),
    SampleSource("Other/2019_4_11_15_25_sitzen_nr3_runter.csv", "sit_down"),
    SampleSource("Sebastian/sit_up.csv", "sit_up"),
    SampleSource("Other/2019_4_11_14_55_sitzen_nr1_hoch.csv", "sit_up"),
    SampleSource("Other/2019_4_11_15_0_sitzen_nr2_hoch.csv", "sit_up"),
    SampleSource("Other/2019_4_11_15_25_sitzen_nr3_hoch.csv", "sit_up"),
    SampleSource("2019_1_15_18_4_Outdoor.csv", "walking"),
    SampleSource("Sebastian/walking.csv", "walking"),
    SampleSource("Sebastian/standing.csv", "standing"),
]

for_cl = ["ir1", "ir3", "ir4", "ir5", "ir6", "fr1", "fr2"]

hyperparams = {
    "C": [2 ** i for i in range(-3, 19, 2)],
    "gamma": [2 ** i for i in range(-21, -2, 2)],
}

default_hyperparams = {"C": 128, "gamma": 3.0517578125e-05}


def score(estimator, X, y):
    global estimated_y
    global a_y
    a_y = y
    estimated_y = estimator.predict(X)

    def get_score_for_class(label) -> float:
        global mask
        global n_correct
        global n_classmembers
        mask = (y == label)
        # [0] because np.where returns a tuple
        n_correct = len(np.where(estimated_y[mask] == y[mask])[0])
        n_classmembers = len(np.where(mask)[0])
        return n_correct / n_classmembers

    return np.average([get_score_for_class(label) for label in unique_labels(y)])


def get_examples():
    # with open_shelve("./cache/saved_session.out") as sh:
    #     if "examples" in sh:
    #         return sh["examples"]

    # ---
    log_time("Get Windows")
    global windows
    windows = load_windows(sources, window_size=64, window_spacing=10, max_time_jump=0.04,
                           max_samples_per_file=None)

    # ---
    log_time("Extract Features from Windows")  #
    examples = pd.DataFrame(
        [extract_features_stats(it.unstack("sensor")[for_cl], n_splits=1)
         for i, it in windows.iterrows()]
    ).set_index(windows.index)

    # ---
    log_time("Cache Examples")
    with open_shelve("./cache/saved_session.out") as sh:
        sh["examples"] = examples

    return examples


if __name__ == "__main__":
    # ---
    log_time("Get Examples")
    examples = get_examples()
    testing_examples, training_examples = stratified_split_by_ratio(1 / 3, examples)

    training_labels = training_examples.index.get_level_values("label")
    testing_labels = testing_examples.index.get_level_values("label")

    # ---
    log_time("Standardize Features")
    training_examples, testing_examples = standardize_per_column(training_examples,
                                                                 testing_examples)
    # ---
    log_time("Find Hyperparams")
    search = GridSearchCV(svm.SVC(), hyperparams, cv=5, n_jobs=-1, error_score="raise", iid=False,
                          refit=False, scoring=score)
    search.fit(training_examples.to_numpy(), training_labels.to_numpy())
    results = search.cv_results_
    best_params = search.best_params_

    # ---
    log_time("Classify / Test")
    svc = svm.SVC(**best_params)
    svc.fit(training_examples.to_numpy(), training_labels.to_numpy())
    predictions = svc.predict(testing_examples.to_numpy())

    score = score(svc, testing_examples.to_numpy(), testing_labels.to_numpy())

    # ---
    log_time("Save Results")
    with open_shelve("./results/saved_session.out") as sh:
        sh["classifier"] = svc
        sh["score"] = score
        sh["results"] = results
        sh["confusion_matrix"] = normalized_confusion_matrix(testing_labels, predictions)

    # ---
    log_time("Plot Results")
    cm = ConfusionMatrix(predictions, testing_labels)
    cm.plot()

    with make_figure(show=False) as ax:
        scores = search.cv_results_["mean_test_score"].reshape(
            len(hyperparams["C"]), len(hyperparams["gamma"])
        )
        ax.pcolor(scores)

    # ---
    log_time("Done")
    plt.show()
    print(results)
