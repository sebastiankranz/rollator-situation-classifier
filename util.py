from collections import namedtuple

import pandas as pd
import time
import matplotlib.pyplot as plt
from typing import Optional, Any, Callable, TypeVar, List, Tuple, Iterable, Sized, Dict

import pywt
from numpy import unravel_index, argsort, argmax
from pandas.tests.extension.numpy_.test_numpy_nested import np
from scipy import signal
from toolz import excepts, curry, compose, pipe
from toolz.curried import mapcat, map, do, take, tail, sorted, merge_sorted
from multiprocessing import Pool

Sin = Tuple[float, float]

T = TypeVar('T')


def safe_run(fun: Callable[..., T], *args: Any) -> Optional[T]:
    try:
        return fun(*args)
    except:
        return None


def default_if_none(default: T, it: Optional[T]) -> T:
    return default if it is None else it


def map_not_none(func):
    def fun(it):
        result = func(it)
        return [result] if result is not None else []

    return mapcat(fun)


filter_none = map_not_none(lambda it: it)


def is_empty(l: List) -> bool:
    return len(l) == 0


def fourier_transform(values: List[float], peaks: int, min_variance: float) -> Iterable[Sin]:
    amps = pipe(np.fft.fft(values),
                np.abs)
    freqs = np.fft.fftfreq(len(values))
    return pipe(zip(amps, freqs),
                filter(lambda a, f: a > min_variance),
                take(accuracy))


concat = curry(pd.concat)


@curry
def assign(columns: Iterable[pd.Series], frame: pd.DataFrame):
    """Assign multiple columns to a dataframe. 
    Columns are replaced based on the name of the Series"""

    df = frame.copy()
    for column in columns:
        df[column.name] = column

    return df


def apply_per_column(func_for_col: Dict[str, Callable[[pd.Series], pd.Series]],
                     frame: pd.DataFrame):
    df = frame.copy()
    for column, func in func_for_col.items():
        df[column] = func(df[column])
    return df


def log_time(label: str):
    print(label + " after: " + str(time.perf_counter()) + "s")


@curry
def enumerate_to_dict(label: str, l: List):
    """
    [A, B, C, D] -> {label0: A, label1: B, label2: C, label3: D}
    """
    return {label + str(i): item for i, item in enumerate(l)}


@curry
def stack_dict(dict: Dict[Any, Dict[Any, T]]) -> Dict[Tuple, T]:
    """
    {A: {a: 1, b: 2}, B: {a: 3}} -> {A_a: 1, A_b: 2, B_a: 3}
    """
    return {str(key) + "_" + str(subkey): item
            for key, subdict in dict.items()
            for subkey, item in subdict.items()}


@curry
def concat_dict(*args) -> Dict:
    new = {}
    for dict in args:
        new.update(dict)
    return new


@curry
def split_list(n_splits: int,
          list: List) -> List[List]:
    length = len(list)
    if length < n_splits:
        raise ValueError("n is bigger than the length of the array")
    base_split_size = length / n_splits
    remainder = length % n_splits
    indices = [(i * base_split_size) + min(i, remainder)
               for i in range(0, n_splits + 1)]
    return [list[i: i + 1] for i in range(0, n_splits)]


@curry
def contains(check: Callable, l):
    return next((it for it in l if check(it)), None) is not None


def save_csv(filename, array, headers):
    np.savetxt("output/" + filename, array, delimiter=",", header=headers, comments="")


def merge_tuple(a, b) -> Tuple:
    first = a if isinstance(a, tuple) else [a]
    second = b if isinstance(b, tuple) else [b]
    return (*first, *second)
