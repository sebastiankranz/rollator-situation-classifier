# %% Setup
from typing import Dict, Iterable, SupportsFloat, Tuple

import numpy as np
import pandas as pd
import pywt

from util import merge_tuple


def wavelet_transform(data: pd.Series, wavelet):
    result = pywt.wavedec(data, wavelet=wavelet)
    final_aprox_coeff = result[0]
    low_passes = result[1:]
    return low_passes, final_aprox_coeff


def get_statistics(values: Iterable) -> Dict[str, SupportsFloat]:
    def crossing_rate(values, line=0):
        adjusted_values = values - line
        return ((adjusted_values[: -1] * adjusted_values[1:]) < 0).sum()

    mean = np.mean(values)
    return {
        "Mean": mean,
        "Variance": np.var(values),
        "Mean Crossing Rate": crossing_rate(values, mean),
        "n25": np.quantile(values, .2),
        "n50": np.quantile(values, .5),
        "n75": np.quantile(values, .7),
        "max": np.max(values),
        "min": np.min(values)
    }


def extract_features_stats(window: pd.DataFrame, n_splits) -> pd.Series:
    split_size = int(len(window) / n_splits)
    splits = [window.iloc[i * split_size : i * split_size + split_size] for i in range(0, n_splits)]

    stats = {
        f"{i}_{column}_{key}": val
        for i, split in enumerate(splits)
        for column in split
        for key, val in get_statistics(split[column]).items()
    }

    return pd.Series(stats)


def extract_features_plain(window: pd.DataFrame) -> pd.Series:
    return window.reset_index(drop=True).stack()


def extract_features_dwt(window: pd.DataFrame, n_peaks,
                         use_stats,
                         use_peaks) -> pd.Series:
    def get_features_for_sensor(window: pd.Series) -> pd.Series:
        low_passes, final_coeff = wavelet_transform(window, wavelet="db2")

        signals = {
            "signal": window.to_numpy(),
            "Ca": final_coeff,
            **({f"Cd{i}": p for i, p in enumerate(low_passes)} if use_stats else {})
        }

        stats = {
            f"{i}_{j}": val
            for i, signal in signals.items()
            for j, val in get_statistics(signal).items()
        }

        points = [
            {"x": x, "level": level, "y": y}
            for level, low_pass in enumerate(low_passes)
            for x, y in enumerate(low_pass)
        ]

        peaks = {
            f"rank{rank}_{key}": val
            for rank, point in enumerate(
                sorted(points, key=lambda p: np.abs(p["y"]), reverse=True)[:n_peaks]
            )
            for key, val in point.items()
        } if use_peaks else {}

        # info = {("info", key): val for key, val in signals.items()}
        features = pd.Series({**stats, **peaks})

        return features

    return pd.concat([get_features_for_sensor(window[col_name]) for col_name in window],
                     keys=window.columns.values, names=["sensor", "feature"])
    # sensors = {
    #     (col_name, key): val
    #     for col_name in window
    #     for key, val in get_features_for_sensor(window[col_name]).items()
    # }
    # return pd.Series(sensors)
