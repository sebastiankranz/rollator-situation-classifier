from typing import List, Dict, Any, Tuple

import pandas as pd
from pathos.pools import ParallelPool
from toolz.curried import mapcat

from preprocessing import split_by_time, generate_windows, apply_window
from util import log_time


class SampleSource:
    def __init__(self, path: str, label: str):
        self.path = path
        self.label = label


def adjust_time(data: pd.DataFrame):
    # print(data)
    start_time = data.at[0, "time"]

    def adjust_column(x):
        return (x - start_time) * 10 ** (-9)

    return data.assign(time=adjust_column(data["time"]))


def read_source(source: SampleSource) -> pd.DataFrame:
    frame = pd.read_csv("./data/" + source.path,
                        index_col=False,
                        skip_blank_lines=True,
                        comment="%",
                        sep=",")
    return adjust_time(frame)


def read_file(path: str, should_adjust_time=True):
    frame = pd.read_csv("./data/" + path,
                        index_col=False,
                        skip_blank_lines=True,
                        comment="%",
                        sep=",")
    if should_adjust_time:
        return adjust_time(frame)
    else:
        return frame

def load_windows(sources,
                 window_size,
                 window_spacing,
                 max_time_jump,
                 max_samples_per_file=None) -> pd.DataFrame:
    def load_windows_for_source(source: SampleSource) -> pd.DataFrame:
        samples = read_source(source).head(max_samples_per_file)

        splitted_samples = split_by_time(max_time_jump, samples)
        windows = mapcat(generate_windows(window_size, window_spacing), splitted_samples)
        windows2 = map(lambda it: it.apply(apply_window)
                       .reset_index(drop=True)
                       .rename_axis("position")
                       .rename_axis("sensor", axis="columns")
                       .stack(), windows)

        log_time(f"Loaded Windows for: {source.path}")
        return pd.DataFrame(windows2).assign(label=source.label)

    return (pd.concat(map(load_windows_for_source, sources), ignore_index=True)
            .set_index(("label", ""), append="true")
            .rename_axis(["example_nr", "label"]))