from typing import Dict, Callable

import pandas as pd
import numpy as np

from util import apply_per_column


def generate_standardization_funcs_for_df(dataframe: pd.DataFrame) -> \
        Dict[str, Callable[[pd.Series], pd.Series]]:
    return {col: generate_standardization_func(dataframe[col])
            for col in dataframe.select_dtypes(include=np.number)}


def generate_standardization_func(reference) -> Callable[[pd.Series], pd.Series]:
    mean = reference.mean()
    std = reference.std() if (reference.std() != 0) else 1

    def apply(new_values: pd.Series = None) -> pd.Series:
        v = new_values if new_values is not None else reference
        return ((v - mean) / std) if std is not 0 else 0

    return apply


def standardize_df(frame: pd.DataFrame) -> pd.DataFrame:
    funcs = generate_standardization_funcs_for_df(frame)
    standardized_df = apply_per_column(funcs, frame)
    return standardized_df


def standardize_per_sensor(training_examples: pd.DataFrame, testing_examples: pd.DataFrame):
    pass


def standardize_per_column(training_examples: pd.DataFrame, testing_examples: pd.DataFrame):
    funcs = generate_standardization_funcs_for_df(training_examples)
    return (apply_per_column(funcs, training_examples), apply_per_column(funcs, testing_examples))
