# %% Setup
# %matplotlib notebook
from contextlib import contextmanager
from typing import Union, List

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from toolz import pipe
from toolz.curried import filter

import classification as cl
import read as r
import schemas
import util
from standardization import generate_standardization_funcs_for_df
from util import is_empty
import model as md
from toolz.curried import map

matplotlib.rc("axes.formatter", useoffset=False)


def show_plot(
    data: pd.DataFrame, columns: Union[List[str], str], title: str = ""
) -> object:
    cols = pipe(columns, filter(lambda it: it in data.columns), list)

    if is_empty(cols):
        return

    data.plot(x="time", y=cols)
    plt.title(title)


def plot_sensordata(data: pd.DataFrame):
    show_plot(data, columns=md.columns[1:7], title="IR")
    show_plot(data, columns=md.columns[7:9] + md.columns[22:24], title="Force and Odo")
    show_plot(data, columns=md.columns[9:13], title="Quat")
    show_plot(data, columns=md.columns[13:16], title="Angular Velocity")
    show_plot(data, columns=md.columns[16:19], title="Linear Acceleration")
    show_plot(data, columns=md.columns[19:22], title="Mag")

def normalized_confusion_matrix(predictions, labels):
    matrix = confusion_matrix(labels, predictions)
    return matrix.astype("float") / matrix.sum(axis=1)[:, np.newaxis]


class ConfusionMatrix:
    def __init__(self, predictions, true_labels):
        self.classes = [str(it) for it in unique_labels(true_labels, predictions)]
        self.matrix = normalized_confusion_matrix(predictions, true_labels)


    def plot(self):
        fig, ax = plt.subplots()
        im = ax.imshow(self.matrix, interpolation="nearest")
        ax.figure.colorbar(im, ax=ax)
        # We want to show all ticks...
        ax.set(
            xticks=np.arange(self.matrix.shape[1]),
            yticks=np.arange(self.matrix.shape[0]),
            # ... and label them with the respective list entries
            xticklabels=self.classes,
            yticklabels=self.classes,
            ylabel="True label",
            xlabel="Predicted label",
        )

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        fmt = ".2f"
        thresh = self.matrix.max() / 2.0
        for i in range(self.matrix.shape[0]):
            for j in range(self.matrix.shape[1]):
                ax.text(
                    j,
                    i,
                    format(self.matrix[i, j], fmt),
                    ha="center",
                    va="center",
                    color="black" if self.matrix[i, j] > thresh else "white",
                )
        fig.tight_layout()
        return ax

    def save_csv(self):
        df = pd.DataFrame(self.matrix, columns=self.classes, index=self.classes)
        df.to_csv("results/confusion_matrix.csv")


@contextmanager
def make_figure(show=True):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    yield ax
    if show:
        plt.show()


def plot_probability_predictions(predictions: pd.DataFrame, onclick):

    with make_figure(show=False) as ax:
        ax.pcolor(predictions.select_dtypes(include=np.number))
        ax.set_xticklabels(predictions.columns.values)
        plt.connect("button_release_event", onclick)


file_names = [
    # "2019_1_15_18_4_Outdoor.txt", "Sebastian/standing.txt",
    # "Sebastian/idle.txt",
    "Sebastian/walking.csv",
    # "Sebastian/2019_1_30_13_45.txt",
    # "Sebastian/step_up.txt",
    # "Sebastian/sit_down.txt", "Sebastian/sit_up.txt",
    # "Other/2019_4_11_15_0_sitzen_nr2.txt", "Other/2019_4_11_15_25_sitzen_nr3.txt"
    # "Sebastian/sit_up.txt"
    # "Sebastian/step_up.txt"
]

if __name__ == "__main__":
    for path in file_names:
        data = r.read_file(path, should_adjust_time=False)
        print(data)
        standardization_funs = generate_standardization_funcs_for_df(
            data.drop("time", axis="columns")
        )
        standardized_data = util.apply_per_column(standardization_funs, data)
        #standardized_data["marked"] = standardized_data["marked"].astype(int)

        show_plot(
            standardized_data,
            ["ir0", "ir1", "ir2", "ir3", "ir4", "fr1", "fr2", "of-interest"],
            title=path,
        )

    plt.show()
