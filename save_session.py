import shelve
from contextlib import contextmanager

location = "./results/saved_session.out"

# def save_session(globals_=None):
#     if globals_ is None:
#         globals_ = globals()
#     my_shelf = shelve.open(location, 'n')
#     for key, value in globals_.items():
#         if not key.startswith('__'):
#             try:
#                 my_shelf[key] = value
#             except Exception:
#                 print('ERROR shelving: "%s"' % key)
#             else:
#                 print('shelved: "%s"' % key)
#     my_shelf.close()
#
# def load_session():
#     my_shelf = shelve.open(location)
#     for key in my_shelf:
#         try:
#             globals()[key]=my_shelf[key]
#         except Exception:
#             print('ERROR restoring: "%s"' % key)
#         else:
#             print('restored: "%s"' % key)
#     my_shelf.close()



@contextmanager
def open_shelve(location):
    s = shelve.open(location)
    yield s
    s.close()