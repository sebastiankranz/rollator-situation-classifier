import matplotlib.pyplot as plt
import pandas as pd


def mark(pos):
    data.at[pos, "marked"] = 1
    data.plot()
    plt.show()
    print(f"Marked {pos}")


columns_to_plot = ["ir1", "ir2", "ir3", "ir4", "ir5", "fr1", "fr2", "marked"]
path = "~/Repositories/rollator-situation-classifier/data/Other/2019_4_11_15_0_sitzen_nr2_runter.csv"

# Backup!!
if __name__ == "__main__":
    data = pd.read_csv(path, index_col=False, skip_blank_lines=True, comment="%")
    print(data)

    if "marked" not in data:
        data = data.assign(marked=False)

    # Convert markers from bool to int, to fix issues with Matplotlib
    data["marked"] = data["marked"].astype(int)

    data[columns_to_plot].plot()
    plt.connect(
        "button_release_event",
        lambda e: mark(int(e.xdata)) if (e.button == 3) else None,
    )      
    plt.show()

    data["marked"] = data["marked"].astype(bool) 

    data.to_csv(path, sep=",", index=False)
    print("Changes Written")
