from typing import List

columns: List[str] = ["ir0", "ir1", "ir2", "ir3", "ir4", "ir5",
                      "fr1", "fr2",
                      "quat-w", "quat-x", "quat-y", "quat-z",
                      "anv-vel-x", "ang-vel-y", "ang-vel-z",
                      "lin-acc-x", "lin-acc-y", "lin-acc-z",
                      "mag-x", "mag-y", "mag-z",
                      "odo1", "odo2"]

